---
layout: handbook-page-toc
title: Buyer Experience Repository
description: >-
  Learn more about the Buyer Experience repository.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

### Technical Architecture

1. **[Nuxt.js](https://nuxtjs.org/):** A vue.js static site generator
2. **Slippers Design System Integration:** SSoT for vue.js components:
3. **Focus Resources:** Devoted to two repositories
    - Digital Experience focused resources on the [Buyer Experience](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience) and [Slippers repositories](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui). 

### Scope
1. Move 80 marketing webpage to the Buyer Experience repository.
    1. Pricing
    1. Homepage
    1. Free Trial
    1. Sales
    1. Demo
    1. Install
    1. Features (Landing and single)
    1. Topics (Landing and single)
    1. Solutions (Landing and single)
    1. Enterprise
    1. SMB
    1. Partners (Landing and single)
    1. Comparison Pages

See the [Marketing Site Information Architecture Refresh epic](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/82) for single source of truth of pages being migrated to this repository. 

# Content Editing Experience 

### Where Content Exists

All content is stored in the [/content](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/tree/main/content) directory and can be accessed via the [Nuxt content module](https://content.nuxtjs.org/)

### How to Edit Content

1. Edit content directly in the browser using [GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html).
1. Alternatively, set up a local development environment and use the [Nuxt content editing tools](https://content.nuxtjs.org/) on your machine. Then commit your changes and make a merge request in GitLab.

# Developer Experience 

### Where Developer Documentation Exists

The [README.md](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience) at the repository root and our [/docs](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/tree/main/docs) directory. 

### How It Works with www-gitlab-com

See [/docs/deployments.md](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/blob/main/docs/deployments.md).  



