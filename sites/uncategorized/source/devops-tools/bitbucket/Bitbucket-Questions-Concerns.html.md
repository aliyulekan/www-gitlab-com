--
layout: markdown_page
title: "Bitbucket Questions and Concerns"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## Questions To Ask When Considering Bitbucket

![GitLab Bitbucket Strengths](/devops-tools/bitbucket/images/BB-Questions.png)

## Concerns You May Have When Migrating From Bitbucket to GitLab

![GitLab Bitbucket Strengths](/devops-tools/bitbucket/images/BB-Concerns.png)
